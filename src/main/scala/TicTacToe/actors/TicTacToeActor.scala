package TicTacToe.actors

import TicTacToe.Models.{Board, Coordinate}
import TicTacToe.views.{BoardView, CoordinateView, GestorIO, TurnView}
import akka.actor.{Actor, ActorRef}

class TicTacToeActor(coordinateController: CoordinateView, player: Char) extends Actor {

  private val _coordinateController : CoordinateView = coordinateController
  private val _player = player

  def receive = {
    case StartMessage(mate) =>
      play(mate, new Board(3))
    case StopMessage =>
      GestorIO.write(s"Parando actor $player\n")
      context.stop(self)
    case GameMessage(game) =>
      play(sender, game)
  }

  private def getFreeCoordinate(board: Board): Coordinate = {
    val freeCoordinate = _coordinateController.read(board.size)
    if (board.canPutOn(freeCoordinate)) {
      freeCoordinate
    } else {
      getFreeCoordinate(board)
    }
  }

  private def getPlayerOriginMoveCoordinate(board: Board): Coordinate = {
    val originCoordinate = _coordinateController.read(board.size)
    if (board.canMoveFrom(originCoordinate, _player)) {
      originCoordinate
    } else {
      getPlayerOriginMoveCoordinate(board)
    }
  }

  private def play(mate: ActorRef, board: Board) = {
    TurnView.write(_player)
    BoardView.write(board)
    if (board.isTicTacToe) {
      GestorIO.write("... pero has perdido\n")
      mate ! StopMessage
      GestorIO.write(s"Parando actor $player\n")
      context.stop(self)
      context.system.terminate()
    } else {
      if (board.canPut(_player)) {
        mate ! GameMessage(board.put(getFreeCoordinate(board), _player))
      } else {
        mate ! GameMessage(board.move(getPlayerOriginMoveCoordinate(board), getFreeCoordinate(board)))
      }
    }
  }
}