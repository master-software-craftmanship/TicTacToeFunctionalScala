package TicTacToe.actors

import TicTacToe.Models.Board
import akka.actor.ActorRef

case class GameMessage(value: Board)
case class StartMessage(respondTo: ActorRef)
case object StopMessage