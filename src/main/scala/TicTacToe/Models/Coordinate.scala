package TicTacToe.Models

class Coordinate(r: Int, c: Int, boardSize: Int) {
  def this(r: Int, c: Int) = this(r, c, 3)

  private val row = this.r
  private val column = this.c
  private val _boardSize = this.boardSize

  def getRow: Int =
    this.row
  def getColumn: Int =
    this.column
  def inDiagonal: Boolean =
    row == column
  def inInverse: Boolean =
    row + column == _boardSize - 1
  def direction(that: Coordinate): String = {
    val dx = that.column - this.column
    val dy = that.row - this.row
    (dx, dy) match {
      case (0, 0) => "stupid"
      case (_, 0) => "horizontal"
      case (0, _) => "vertical"
      case _ if that.inDiagonal && this.inDiagonal => "diagonal"
      case _ if that.inInverse && this.inInverse => "inverse"
      case _ => ""
    }
  }
  //Horizontal, diagonal, vertical, inversa

  override def equals(that: Any): Boolean =
    that match {
      case that: Coordinate => that.row.equals(this.row) && that.column == this.column
      case _ => false
    }

  override def toString: String =
    "TicTacToe.Models.Coordinate(" + row + ", " + column + ")"
}

/*
0,0 0,1 0,2
1,0 1,1 1,2
2,0 2,1 2,2
 */

