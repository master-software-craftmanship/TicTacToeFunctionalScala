package TicTacToe.Models

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._

object Board {
  val emptyChar = '.'
  private def getSizedList[A](numberOfElements: Int, initialValue: A): List[A] =
    numberOfElements match {
      case 0 => Nil
      case it => initialValue::getSizedList(it - 1, initialValue)
    }
  private def getInitialState(boardSize: Int) = getSizedList(boardSize, getSizedList(boardSize, emptyChar))
}

class Board(bState: List[List[Char]], numberOfPlayers: Int) {
  def this(boardSize: Int, numberOfPlayers: Int) = this(Board.getInitialState(boardSize), numberOfPlayers)
  def this(boardSize: Int) = this(boardSize, 2)

  private val boardState = bState
  private val nPlayers = numberOfPlayers

  def put(coordinate: Coordinate, player: Char): Board =
    new Board(boardState.zipWithIndex.map {
      case (row, index) => if (index == coordinate.getRow) this.putOnRow(row, coordinate.getColumn, player) else row
    }, this.nPlayers)

  def move(coordinateOrigin: Coordinate, coordinateDestiny: Coordinate): Board =
    new Board({
      val originColor = getColor(coordinateOrigin)
      val boardSize = boardState.size
      val originIndex = coordinateOrigin.getRow * boardSize + coordinateOrigin.getColumn
      val destinyIndex = coordinateDestiny.getRow * boardSize + coordinateDestiny.getColumn
      boardState.flatten.updated(originIndex, Board.emptyChar).updated(destinyIndex, originColor).sliding(boardSize, boardSize).toList
    }, this.nPlayers)

  def playerWins(player: Char): Boolean =
    canMove(player) && isOnRowForPlayer(player)

  def isTicTacToe: Boolean = {
    playerColors.map(x => Future {
      playerWins(x)
    }).foldLeft(false)(_ || Await.result(_, 1 seconds))
  }

  def canPut(player: Char): Boolean = {
    val tokensOfPlayer = getPositionsOfPlayer(player).size
    val canAddNewPlayer = playerColors.count(_ != player) < nPlayers
    canAddNewPlayer && !canMove(player) && (tokensOfPlayer == 0 || playerColors.exists(_ != player) && !playerColors.filter(_ != player).map( tokensOfPlayer > getPositionsOfPlayer(_).size ).foldLeft(false)(_ || _))
  }

  def canMove(player: Char): Boolean =
    getPositionsOfPlayer(player).size == this.size

  def canPutOn(coordinate: Coordinate): Boolean =
    getColor(coordinate) == Board.emptyChar

  def canMoveFrom(coordinate: Coordinate, player: Char): Boolean =
    getColor(coordinate) == player

  def getColor(coordinate: Coordinate): Char =
    boardState(coordinate.getRow)(coordinate.getColumn)

  def size: Int =
    boardState.size

  private def isOnRowForPlayer(player: Char): Boolean = {
    val m1::m2::playerMovements = getPositionsOfPlayer(player)
    val comparingDirection = m1.direction(m2)
    comparingDirection != "" && playerMovements.forall(_.direction(m1) == comparingDirection)
  }

  private def playerColors: List[Char] =
    boardState.flatten.distinct.filter(_ != Board.emptyChar)

  private def putOnRow(row: List[Char], col: Int, player: Char): List[Char] =
    row.zipWithIndex.map { case (char, index) => if (index == col) player else char }

  def getPositionsOfPlayer(player: Char): List[Coordinate] =
    boardState.zipWithIndex.flatMap {
      case (row, index) => getPositionsOfPlayerInRow(row, index, player)
    }

  private def getPositionsOfPlayerInRow(row: List[Char], rowIndex: Int, player: Char): List[Coordinate] =
    row.zipWithIndex.filter({
      case (c, _) if c == player => true
      case _ => false
    }).map({
      case (_, index) => new Coordinate(rowIndex, index)
    })

  override def equals(that: Any): Boolean =
    that match {
      case that: Board => that.boardState.equals(this.boardState)
      case _ => false
    }

  override def toString: String =
    "TicTacToe.Models.Board(boardState=" + boardState.toString + ", numberOfPlayers=" + this.nPlayers + ")"

}
