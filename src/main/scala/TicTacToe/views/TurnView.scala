package TicTacToe.views

object TurnView {

  def write(player: Char) = {
    GestorIO.write(s"Es el turno de $player\n")
  }

}
