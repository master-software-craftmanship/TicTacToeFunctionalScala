package TicTacToe.views

import TicTacToe.views.Automatic.AutomaticCoordinateView
import TicTacToe.views.Manual.ManualCoordinateView

object CoordinateControllerView {

  def read:CoordinateView = {
    val playerKind = GestorIO.readChar("[A]utomatico o [M]anual? [A,M]")
    playerKind match  {
      case 'A' => AutomaticCoordinateView
      case 'M' => ManualCoordinateView
    }

  }

}
