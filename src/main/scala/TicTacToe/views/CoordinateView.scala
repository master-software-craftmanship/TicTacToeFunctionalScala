package TicTacToe.views

import TicTacToe.Models.Coordinate


trait CoordinateView {

  def read(boardSize: Int):Coordinate

}
