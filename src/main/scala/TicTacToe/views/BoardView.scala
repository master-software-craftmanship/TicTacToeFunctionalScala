package TicTacToe.views

import TicTacToe.Models.{Board, Coordinate}

object BoardView {

  def write(board: Board) = {
    var result = ""
    for (i <- List(0, 1, 2)) {
      for (j <- List(0, 1, 2)) {
        result += board.getColor(new Coordinate(i, j))
      }
      result += "\n"
    }
    println(result)
  }
}
