package TicTacToe.views.Automatic

import TicTacToe.Models.Coordinate
import TicTacToe.views.CoordinateView

import scala.util.Random

object AutomaticCoordinateView extends CoordinateView {
  private var randomGenerator = new Random()

  def read(boardSize: Int):Coordinate = {
    val row = randomGenerator.nextInt(boardSize)
    val column = randomGenerator.nextInt(boardSize)
    new Coordinate(row, column)
  }

}
