package TicTacToe.views.Manual

import TicTacToe.Models.Coordinate
import TicTacToe.views.{CoordinateView, GestorIO}

object ManualCoordinateView extends CoordinateView {

  def read(boardSize: Int):Coordinate = {
    val row = GestorIO.readInt(s"Fila? [1-$boardSize]")
    val column = GestorIO.readInt(s"Columna? [1-$boardSize]")
    new Coordinate(row-1, column-1)
  }

}
