package TicTacToe

import TicTacToe.actors.{StartMessage, TicTacToeActor}
import TicTacToe.views.CoordinateControllerView
import akka.actor.{ActorSystem, Props}

object Main extends App {
  val coordinateControler = CoordinateControllerView.read
  val system = ActorSystem("system")
  val actorX = system.actorOf(Props(classOf[TicTacToeActor], coordinateControler, 'x'), "actorX")
  val actorO = system.actorOf(Props(classOf[TicTacToeActor], coordinateControler, 'o'), "actorO")

  actorX ! StartMessage(actorO)
}
