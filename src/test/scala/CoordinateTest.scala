import TicTacToe.Models.Coordinate
import org.scalatest.FunSuite

class CoordinateTest extends FunSuite {
 test("diagonal 1") {
   val position = 0
   val coordinate = new Coordinate(position,position)
   assert(coordinate.inDiagonal)
   assert(!coordinate.inInverse)
  }
  test("diagonal 2") {
    val position = 1
    val coordinate = new Coordinate(position,position)
    assert(coordinate.inDiagonal)
    assert(coordinate.inInverse)
  }
  test("diagonal 3") {
    val position = 2
    val coordinate = new Coordinate(position,position)
   assert(coordinate.inDiagonal)
   assert(!coordinate.inInverse)
  }
  test("direction diagonal") {
    val coordinate = new Coordinate(0,0)
    val coordinate2 = new Coordinate(2,2)
    assert(coordinate.direction(coordinate2) == "diagonal")
    assert(coordinate2.direction(coordinate) == "diagonal")
  }
  test("direction inverse") {
    val coordinate = new Coordinate(0,2)
    val coordinate2 = new Coordinate(2,0)
    assert(coordinate.direction(coordinate2) == "inverse")
    assert(coordinate2.direction(coordinate) == "inverse")
  }
  test("direction horizontal") {
    val coordinate = new Coordinate(1,2)
    val coordinate2 = new Coordinate(1,0)
    assert(coordinate.direction(coordinate2) == "horizontal")
    assert(coordinate2.direction(coordinate) == "horizontal")
  }
  test("direction vertical") {
    val coordinate = new Coordinate(1,1)
    val coordinate2 = new Coordinate(0,1)
    assert(coordinate.direction(coordinate2) == "vertical")
    assert(coordinate2.direction(coordinate) == "vertical")
  }
  test("direction stupid") {
    val coordinate = new Coordinate(1,1)
    assert(coordinate.direction(coordinate) == "stupid")
  }
  test("direction empty") {
    val coordinate = new Coordinate(1,2)
    val coordinate2 = new Coordinate(2,1)
    assert(coordinate.direction(coordinate2) == "")
    assert(coordinate2.direction(coordinate) == "")
  }
}
