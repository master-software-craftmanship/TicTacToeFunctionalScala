import TicTacToe.Models.{Board, Coordinate}
import org.scalatest.FunSuite

class BoardTest extends FunSuite {
  test("put 1") {
    val size = 3
    val board = new Board(size)
    assert(board.size == size)
    val board1 = board.put(new Coordinate(1, 1), 'x')
    assert(!board1.canPutOn(new Coordinate(1, 1)))
  }
  test("playerPositions 1") {
    val size = 3
    val board = new Board(size)
    assert(board.size == size)
    val board1 = board.put(new Coordinate(1, 1), 'x')
    assert(board1.getPositionsOfPlayer('x').size == 1)
    assert(board1.getPositionsOfPlayer('x').equals(List(new Coordinate(1, 1))))
    val board2 = board1.put(new Coordinate(0,0), 'x')
    assert(board2.getPositionsOfPlayer('x').size == 2)
    assert(board2.getPositionsOfPlayer('x').equals(List(new Coordinate(0, 0), new Coordinate(1, 1))))
  }
  test("move 1") {
    val size = 3
    val board1 = new Board(size).put(new Coordinate(1, 1), 'x').move(new Coordinate(1, 1), new Coordinate(0, 0))
    val board2 = new Board(size).put(new Coordinate(0, 0), 'x')
    assert(board1 == board2)
  }
  test("can put 1") {
    val size = 3
    val board = new Board(size)
    assert(board.canPut('x'))
    assert(board.canPut('o'))
    val board1 = board.put(new Coordinate(1, 1), 'x')
    assert(!board1.canPut('x'))
    assert(board1.canPut('o'))
    val board2 = board1.put(new Coordinate(0, 0), 'o')
    assert(board2.canPut('x'))
    assert(board2.canPut('o'))
    assert(!board2.canPut('i'))
    val board3 = board2.put(new Coordinate(1, 0), 'o')
    assert(board3.canPut('x'))
    assert(!board3.canPut('o'))
    val board4 = board3.put(new Coordinate(0, 1), 'x').put(new Coordinate(2, 1), 'x').put(new Coordinate(2, 0), 'o')
    assert(!board4.canPut('o'))
    assert(!board4.canPut('x'))
  }
  test("can put 2") {
    val size = 4
    val board = new Board(size)
    assert(board.canPut('x'))
    assert(board.canPut('o'))
    val board1 = board.put(new Coordinate(1, 1), 'x')
    assert(!board1.canPut('x'))
    assert(board1.canPut('o'))
    val board2 = board1.put(new Coordinate(0, 0), 'o')
    assert(board2.canPut('x'))
    assert(board2.canPut('o'))
    val board3 = board2.put(new Coordinate(1, 0), 'o')
    assert(board3.canPut('x'))
    assert(!board3.canPut('o'))
    val board4 = board3.put(new Coordinate(0, 1), 'x').put(new Coordinate(2, 1), 'x').put(new Coordinate(2, 0), 'o')
    assert(board4.canPut('o'))
    assert(board4.canPut('x'))
  }

}
